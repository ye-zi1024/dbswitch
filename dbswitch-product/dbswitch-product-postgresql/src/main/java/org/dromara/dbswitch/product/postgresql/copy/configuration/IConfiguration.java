package org.dromara.dbswitch.product.postgresql.copy.configuration;

public interface IConfiguration {

  int getBufferSize();
}
