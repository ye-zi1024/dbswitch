// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package org.dromara.dbswitch.admin.mapper;

import org.dromara.dbswitch.admin.entity.AssignmentConfigEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AssignmentConfigMapper extends BaseMapper<AssignmentConfigEntity> {

}
